
import React, { useState, useEffect } from "react";
import {
    StyleSheet,
    Text,
    View,
    Modal,
    Pressable,
    Dimensions,
    ToastAndroid
} from "react-native";
import {
    WheelPicker,
} from "mahdi-wheel";
import moment from 'moment-jalaali';

const { width } = Dimensions.get('window')


const DatePicker = ({
    onRequestClose,
    onPressAction,
    visible = false,
    backgroundColor = 'green',
    mainBackgroundColor = 'white',
    textColor = 'white',
    fontFamily = undefined,
    fontSize = 16,
    selectedItemTextColor = 'black',
    itemTextColor = 'grey',
    selectedItemBorderColor = 'white',
    future = false,
    futureMaxYear = 20,
    beforeMaxYear = 50,
    borderRadius = 10
}) => {
    const [mainData, setMainData] = useState([]);
    const [month, setMonth] = useState([]);
    const [year, setYear] = useState([]);
    const [day, setDay] = useState([]);
    const [selectedYear, setSelectedYear] = useState('1350');
    const [selectedItem, setSelectedItem] = useState(0);            // Month Picked DatePicker
    const [selectedItemYear, setSelectedItemYear] = useState(0);    // Year Picked Index
    const [selectedItemDay, setSelectedItemDay] = useState(0);      // Day Picked Index


    useEffect(() => {
        if (visible) {
            setSelectedItem(0)
            setSelectedItemYear(0)
            setSelectedItemDay(0)
            _createDates()
        }
        return () => {

        }
    }, [visible, future, futureMaxYear, beforeMaxYear])

    const _createDates = async () => {
        var MaxMonth;
        var MaxYear;
        var MaxDay;

        let maxDate = moment();
        maxDate.add(future ? futureMaxYear : beforeMaxYear, 'jYear');
        var MaxYear = maxDate.jYear();
        var MaxMonth = 12;
        if (moment.jIsLeapYear(_year)) {
            var MaxDay = 30;
        } else {
            var MaxDay = 29;
        }

        let m = moment();
        var month = m.jMonth();
        var year = future ? m.jYear() + 0 : m.jYear() - beforeMaxYear;
        var day = m.jDate();

        let data = [];

        for (let i = 0; m.jYear() <= MaxYear; i++) {
            var _year = future ? m.jYear() + 0 : m.jYear() - beforeMaxYear;
            let months = [];
            for (let j = 0; j < 12; j++) {
                var daysLength = moment.jIsLeapYear(_year) ? 30 : 29;
                if (j != 11) {
                    daysLength = 30;
                }
                if (j <= 5) {
                    daysLength = 31;
                }
                if (_year == MaxYear && j > MaxMonth) break;
                if (year == _year && j < month) continue;
                let days = [];
                for (let k = 0; k < daysLength; k++) {
                    if (_year == MaxYear && j == MaxMonth && k == MaxDay) break;
                    if (_year == year && month == j && k < day - 1) continue;
                    days.push(k + 1);
                }
                let _days = {};
                _days[getMonthString(j + 1)] = days;
                months.push(_days);
            }
            let _data = {};
            _data[_year] = months;
            data.push(_data);
            m.add(1, 'jYear');
        }


        let temp1 = []
        data.map((i, ind) => {
            Object.keys(i).map((key, index) => {
                temp1.push(key)
            })
        })
        setYear(temp1)
        let temp = []
        data[0][Object.keys(data[0])[0]].map((i, ind) => {
            Object.keys(i).map((key, index) => {
                temp.push(key)
            })
        })

        // console.log();
        setMonth(temp)
        let temp2 = []
        data[0][Object.keys(data[0])[0]][0][Object.keys(data[0][Object.keys(data[0])[0]][0])[0]].map((i, ind) => {
            temp2.push(i.toString())
        })
        setSelectedYear(Object.keys(data[0])[0])
        // console.log('test', temp2);
        setDay(temp2)
        setMainData(data)

    };


    const getMonthString = number => {
        switch (number) {
            case 1:
                return 'فروردین';
            case 2:
                return 'اردیبهشت';
            case 3:
                return 'خرداد';
            case 4:
                return 'تیر';
            case 5:
                return 'مرداد';
            case 6:
                return 'شهریور';
            case 7:
                return 'مهر';
            case 8:
                return 'آبان';
            case 9:
                return 'آذر';
            case 10:
                return 'دی';
            case 11:
                return 'بهمن';
            case 12:
                return 'اسفند';
        }
        return number;
    };

    const yearChange = (index, year) => {
        setSelectedItemDay(0)
        setSelectedItem(0)
        let temp = []
        mainData[index][year].map((i, ind) => {
            Object.keys(i).map((key, index) => {
                temp.push(key)
            })
        })



        setMonth(temp)

        let temp2 = []
        Object.values(mainData[index][year][selectedItem]).map((i, ind) => {
            temp2 = i
        })
        let tempTest = []

        temp2.map((i, ind) => {
            tempTest.push(i.toString())
        })
        setDay(tempTest)
    }

    const onMonthChange = (index, month) => {
        setSelectedItemDay(0)
        let temp2 = []
        mainData[selectedItemYear][selectedYear][index][month].map((i, ind) => {
            temp2.push(i.toString())
        })
        setDay(temp2)
    }

    const getMonthNumber = string => {
        switch (string) {
            case 'فروردین':
                return 1;
            case 'اردیبهشت':
                return 2;
            case 'خرداد':
                return 3;
            case 'تیر':
                return 4;
            case 'مرداد':
                return 5;
            case 'شهریور':
                return 6;
            case 'مهر':
                return 7;
            case 'آبان':
                return 8;
            case 'آذر':
                return 9;
            case 'دی':
                return 10;
            case 'بهمن':
                return 11;
            case 'اسفند':
                return 12;
        }
        return 0;
    };

    const confirm = () => {
        let returnData = {
            date: moment(`${selectedYear}/${getMonthNumber(month[selectedItem])}/${day[selectedItemDay]}`, 'jYYYY/jM/jD HH:mm').format('YYYY-M-D HH:mm:ss'),
            persianDate: `${selectedYear} / ${month[selectedItem]} / ${day[selectedItemDay]}`,
        }
        onPressAction(returnData)
    }

    if (itemTextColor.includes('#') || selectedItemTextColor.includes('#')) {
        ToastAndroid.show('dont use hex color for items', ToastAndroid.SHORT)
        return (<></>)
    }

    return (
        <Modal
            animationType="fade"
            transparent={true}
            duration={200}
            visible={visible}
            onRequestClose={onRequestClose}
        >
            <Pressable onPress={onRequestClose} style={styles.container}>
                <View style={[styles.header, { borderTopRightRadius: borderRadius, borderTopLeftRadius: borderRadius, backgroundColor: backgroundColor, }]}>
                    <Text style={{ color: textColor, fontSize: fontSize, fontFamily: fontFamily }}>انتخاب تاریخ</Text>
                </View>
                <Pressable onPress={() => { }} style={[styles.main, { backgroundColor: mainBackgroundColor }]}>

                    {/* Year Picker */}


                    <WheelPicker
                        indicatorColor={selectedItemBorderColor}
                        itemTextColor={itemTextColor}
                        selectedItemTextColor={selectedItemTextColor}
                        itemTextFontFamily={fontFamily}
                        selectedItemTextFontFamily={fontFamily}
                        selectedItem={selectedItemYear}
                        style={{ height: 130, width: width / 3 - 25 }}
                        data={year.length == 0 ? [] : year}
                        onItemSelected={(select) => { setSelectedItemYear(select); setSelectedYear(year[select]); yearChange(select, year[select]) }}
                    />

                    {/* Month Picker */}


                    <WheelPicker
                        indicatorColor={selectedItemBorderColor}
                        itemTextColor={itemTextColor}
                        selectedItemTextColor={selectedItemTextColor}
                        itemTextFontFamily={fontFamily}
                        selectedItemTextFontFamily={fontFamily}
                        style={{ height: 130, width: width / 3 - 25 }}
                        selectedItem={selectedItem}
                        data={month.length == 0 ? [] : month}
                        onItemSelected={(select) => { setSelectedItem(select); onMonthChange(select, month[select]) }}
                    />

                    {/* Day Picker */}



                    <WheelPicker
                        indicatorColor={selectedItemBorderColor}
                        itemTextColor={itemTextColor}
                        selectedItemTextColor={selectedItemTextColor}
                        itemTextFontFamily={fontFamily}
                        selectedItemTextFontFamily={fontFamily}
                        selectedItem={selectedItemDay}
                        style={{ height: 130, width: width / 3 - 25 }}
                        data={day.length == 0 ? [] : day}
                        onItemSelected={(select) => setSelectedItemDay(select)}
                    />
                </Pressable>
                <View style={{ borderBottomRightRadius: borderRadius, borderBottomLeftRadius: borderRadius, backgroundColor: mainBackgroundColor, width: '100%' }}>
                    <Pressable onPress={confirm} style={[styles.submitBtn, { backgroundColor: backgroundColor, }]}>
                        <Text style={{ color: textColor, fontSize: fontSize, fontFamily: fontFamily }}>تایید</Text>
                    </Pressable>
                </View>
            </Pressable>

        </Modal >
    )

};

export default DatePicker;

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 10,
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.5)',
        justifyContent: 'center',
        alignItems: 'center',
    },
    main: {
        paddingHorizontal: 5,
        width: '100%',
        height: 200,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    submitBtn: {
        margin: 10,
        borderRadius: 10,
        alignSelf: 'center',
        width: 80,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center'
    },
    header: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        height: 50,
    },
})